
#Oblig5 - Ruteplanlegging, den siste obligen i INF1000h2013. Finn raske reiser med Trikk og T-bane i Oslo.

![UML](http://f.cl.ly/items/2J1v0O0m2N292q3E0B46/UML.png)

##Formål

Det er 6 T-banelinjer (1,2,3,4,5 og 6) og 6 trikkelinjer (11,12,13,17,18 og 19) i Oslo. På mange av stoppestedene/stasjonene, men ikke på alle, er det mulig å gå over til en stasjon med samme navn på en annen linje. Eksempel på slike stasjoner med mange overganger er Forskningsparken, Majorstua, Storo og Jernbanetorget. Alle opplysningene vi skal bruke om linjer og stasjoner ligger på en fil. Vitsen med denne planleggeren er ikke å finne eksakt reisetid for en bestemt avgang, men finne den jevnt over beste reisemåte mellom to stasjoner med maksimalt én overgang.

I denne obligen skal du derfor først lese inn fila: «TrikkOgTbane.txt» med alle t-bane-linjer og trikkelinjer i Oslo, og så kunne regne ut alternative måter å reise fra en gitt stasjon/holdeplass på en trikk- eller t-bane til en annen gitt stasjon ved å gjøre maksimalt én omstigning til en annen t-bane eller trikke-linje. Hvordan vi gjør det er nærmere beskrevet nedenfor. Vi skal også kunne regne ut en sannsynlig reisetid for de ulike reiserutene, og som en siste oppgave kunne skrive ut en forventet raskeste reise med bare én eller ingen omstigning. Vi kan tenke oss at det du lager er en første skisse til en ruteplanlegger for Ruter, som jo driver disse linjene. Vi skal se bort fra busser i denne oppgaven, men det vil relativt lett kunne legges inn, når du først har fått t-baner og trikker til å virke.