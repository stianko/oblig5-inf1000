import java.util.*;
import java.io.*;


class Oblig5{
    public static void main(String[] args){
	//Oppretter objektet pl
	Planlegger pl = new Planlegger();
    }
}

//Klasse som inneholder fillesing, spoerring til bruker og ruteberegninger
class Planlegger{
    Scanner sc = null;
    Scanner tast = new Scanner(System.in);
    String filnavn = "trikkogtbaneutf8-ver2.txt";
    String fra, til;
    String lesing;
    int linjenr;
    double totTid, reiseTid1, reiseTid2, vent;
    double kortestTid = 10000;
    double kortestDir = 10000;
    Linje linje = null;
    Stasjon stasjon = null;
    //Hashmap som inneholder alle stasjoner
    HashMap<String, Stasjon> stasjonHm = new HashMap<String, Stasjon>();

    //Leser fil som inneholder alle ruter
    void lesFil(){
	try{
	    sc = new Scanner(new File(filnavn));
	    while(sc.hasNext()){
		//leser og fjerner '-' i teksten
		lesing = sc.next().replace("-", " ").toLowerCase();
		//Oppretter nye linje-objekter naar disse forekommer i tekstfilen
		if(lesing.equals("*linje*")){
		    linjenr = sc.nextInt();
		    linje = new Linje(linjenr);
		} else {
		    //Oppretter stasjonsobjekter om disse ikke finnes fra foer,
		    //og legger den til i en arraylist i linjeobjektet vi fortsatt er i.
		    //Eksisterer stasjonen i en annen linje, legges den bare til i arraylisten.
		    if(!stasjonHm.containsKey(lesing)){
			stasjon = new Stasjon(lesing);
			stasjonHm.put(lesing, stasjon);
			stasjon.stasjonsLinjer.put(linjenr, linje);
			linje.linjeStasjoner.add(stasjon);
		    } else {
			linje.linjeStasjoner.add(stasjonHm.get(lesing));
			stasjonHm.get(lesing).stasjonsLinjer.put(linjenr, linje);
		    }
		}
	    }
	    sc.close();
	} catch (Exception e){
	    System.out.println("Kunne ikke lese filen " + filnavn);
	}
    }

    //Spoerr bruker om reiserute
    void lesFraTil(){
	System.out.println("Reise fra: ");
	fra = tast.nextLine().toLowerCase();
	System.out.println("Reise til: ");
	til = tast.nextLine().toLowerCase();
	//Avslutter programmet om bruker spoerr om tilfeller som ikke er mulige (Ringen er retning, ikke stasjon)
	if (fra.equalsIgnoreCase("Ringen") || til.equalsIgnoreCase("Ringen")){
	    System.out.println("\nRingen er ikke en stasjon");
	} else if (fra.equals(til)){
	    System.out.println("DOES NOT COMPUTE");
	} else if (!stasjonHm.containsKey(fra) || !stasjonHm.containsKey(til)){
	    if(!stasjonHm.containsKey(fra)){
		System.out.println(fra + " er ikke en stasjon");
	    } else {
		System.out.println(til + " er ikke en stasjon");
	    }
	} else {
	    //Kaller paa beregning om begge stasjonene eksisterer og ikke er like
	    beregnRuter(stasjonHm.get(fra), stasjonHm.get(til));
	    System.out.println();
	}
    }
    //Beregner ruter, kaller overgang om ingen direkteruter eksisterer
    void beregnRuter(Stasjon fraStj, Stasjon tilStj){
	boolean direkte = false;
	String kortType = "";
	int kortNr= 0;
	String kortEnde = "";
	double kortDir = 0;
	
	System.out.println("Du kan ta foelgende ruter: ");
	//Itererer gjennom HashMap-et i Stasjon og printer ut alle linjer som inneholder baade fraStj og tilStj
	for(Linje fraLin : fraStj.stasjonsLinjer.values()){
	    if(fraLin.inneholder(tilStj)){
		totTid = fraLin.tid(fraStj, tilStj, fraLin.linjeNr);
		System.out.printf("Ta %s nr. %d mot %s, reisetid %.1f minutter\n", fraLin.type(fraLin.linjeNr), fraLin.linjeNr, fraLin.retning(fraStj, tilStj), totTid);
		direkte = true;
		//Lagrer korteste rute
		if(totTid < kortestDir){
		    kortType = fraLin.type(fraLin.linjeNr);
		    kortNr = fraLin.linjeNr;
		    kortEnde = fraLin.retning(fraStj, tilStj);
		    kortestDir = totTid;
		}
	    }
	}
	//Kaller paa metode for overgang om ruten ikke er direkte
	if (!direkte){
	    overgang(fraStj, tilStj);
	} else {
	    //Skriver ut korteste direkte-rute
	    System.out.println("direkte.\n-------------------------------------------\nKorteste rute er:");
	    System.out.printf("Ta %s nr. %d mot %s, reisetid %.1f minutter\n", kortType, kortNr, kortEnde, kortestDir);
	    
	}
    }
    
    //Beregner og skriver ut om ruten ikke er direkte
    void overgang(Stasjon fraStj, Stasjon tilStj){
	String fraType, kortFraType = "";
	int fraLinje, kortFraLinje = 0;
	String endeStasjonFra, kortEndeStasjonFra = "";
	String viaStasjon, kortViaStasjon = "";
	double overgangsTid, kortOvergangsTid = 0;
	String viaType, kortViaType = "";
	int viaLinje, kortViaLinje = 0;
	String viaEndeStasjon, kortViaEndeStasjon = "";
	int TilStasjon, kortTilStasjon = 0;

	//Itererer gjennom alle linjene paa stasjonen man reiser fra
	for(Linje fraLin : fraStj.stasjonsLinjer.values()){
	    //Itererer gjennom alle stasjonene som ligger pr. linje paa fra-stasjon
	    for(Stasjon viaStj : fraLin.linjeStasjoner ){
		//Itererer gjennom alle linjene pr. stasjon i forrige iterasjon for aa finne endestasjon
		for(Linje tilLin : viaStj.stasjonsLinjer.values()){
		    //Lagrer variable og kaller metode for utskrift naar stasjon er funnet,
		    //unntak for ringen som ikke er stasjon
		    if(tilLin.inneholder(tilStj) && !viaStj.navn.equalsIgnoreCase("Ringen")){
			reiseTid1 = fraLin.tid(fraStj, viaStj, fraLin.linjeNr);
			reiseTid2 = tilLin.tid(viaStj, tilStj, tilLin.linjeNr);
			vent = fraLin.venteTid(tilLin.linjeNr);
			totTid = reiseTid1 + reiseTid2 + vent + 3;
			fraType = fraLin.type(fraLin.linjeNr);
			fraLinje = fraLin.linjeNr;
			endeStasjonFra = fraLin.retning(fraStj, viaStj);
			viaStasjon = viaStj.navn;
			overgangsTid = fraLin.venteTid(tilLin.linjeNr);
			viaType = tilLin.type(tilLin.linjeNr);
			viaLinje = tilLin.linjeNr;
			viaEndeStasjon = tilLin.retning(viaStj, tilStj);
			//Lagrer korteste rute
			if (totTid < kortestTid){
			    kortestTid = totTid;
			    kortFraType = fraType;
			    kortFraLinje = fraLinje;
			    kortEndeStasjonFra = endeStasjonFra;
			    kortViaStasjon = viaStasjon;
			    kortOvergangsTid = overgangsTid;
			    kortViaType = viaType;
			    kortViaLinje = viaLinje;
			    kortViaEndeStasjon = viaEndeStasjon;
			}
			//Kaller metode utskrift
			utskrift(fraType, fraLinje, endeStasjonFra, viaStasjon, overgangsTid, viaType, viaLinje, viaEndeStasjon, totTid);
		    }
		}
	    }
	}
	System.out.println("\n-------------------------------------------");
	System.out.println("Korteste reiserute er: ");
	utskrift(kortFraType, kortFraLinje, kortEndeStasjonFra, kortViaStasjon, kortOvergangsTid, kortViaType, kortViaLinje, kortViaEndeStasjon, kortestTid);
    }

    //Skriver ut reiseruter
    void utskrift(String fraType, int fraLinje, String endeStasjon, String viaStasjon, double overgangsTid, String viaType, int viaLinje, String viaEndeStasjon, double totTid){
	System.out.printf("Ta %s nr. %d mot %s til %s,\nvent %.1f minutter\n", fraType, fraLinje, endeStasjon, viaStasjon, overgangsTid);
	System.out.printf("Ta %s nr. %d mot %s\n", viaType, viaLinje, viaEndeStasjon);
	System.out.printf("Total reisetid: %.1f minutter.\n\n", totTid);
    }
    
    //Konstruktoer    
    Planlegger(){
	lesFil();
	lesFraTil();
    }
}

//Linjeklasse, inneholder ArrayList med alle stasjoner pr. linjeobjekt
class Linje{
    double reiseTid;
    int antStasjoner;
    int linjeNr;
    ArrayList<Stasjon> linjeStasjoner = new ArrayList<Stasjon>();
    //Konstruktoer
    Linje(int ln){
	this.linjeNr = ln;
    }

    //Sjekker om stasjonen eksisterer paa linja
    boolean inneholder(Stasjon s){
	if(linjeStasjoner.contains(s)){
	    return true;
	} else {
	    return false;
	}
    }

    //Returnerer reisemaate, t-bane eller trikk
    String type (int nr){
	if (nr < 7){
	    return "t-bane";
	} else {
	    return "trikk";
	}
    }

    //Returnerer navn paa endestasjon basert paa retning
    String retning (Stasjon fra, Stasjon til) {
	String endeStasjon = "";
	int fraStasjon, tilStasjon;
	fraStasjon = linjeStasjoner.indexOf(fra);
	tilStasjon = linjeStasjoner.indexOf(til);
	if (fraStasjon < tilStasjon){
	    endeStasjon = linjeStasjoner.get(linjeStasjoner.size() - 1).navn;
	} else {
	    endeStasjon = linjeStasjoner.get(0).navn;
	}
	return endeStasjon;
    }

    //Returnerer ventetid for trikk eller t-bane
    double venteTid(int nr){
	if (nr < 7){
	    return 7.5;
	} else {
	    return 5;
	}
    }

    //Returnerer positiv reisetid for en strekning
    double tid(Stasjon fra, Stasjon til, int type){
	int fraInd = linjeStasjoner.indexOf(fra);
	int tilInd = linjeStasjoner.indexOf(til);
	double reisetid = tilInd - fraInd;
	if (type < 7){
	    reisetid *= 1.8;
	} else {
	    reisetid *= 1.4;
	}
	return Math.abs(reisetid);
    }
}
//Stasjons-klasse, inneholder HashMap med alle linjene som gaar igjennom stasjonen
class Stasjon{
    String navn;
    HashMap<Integer, Linje> stasjonsLinjer = new HashMap<Integer, Linje>();

    //Konstruktoer
    Stasjon(String n){
	this.navn = n;
    }
}
